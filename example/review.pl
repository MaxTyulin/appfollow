#!/usr/bin/env perl

use 5.012;
use warnings;
use lib::abs '../lib';
use GooglePlay::ReviewBeta;
use Data::Dumper;

my $app;
#$app = 'com.yandex.browser';
$app = 'com.aita';

my $reviews = GooglePlay::ReviewBeta->new($app);

my $review_list = $reviews->retrieve({page=>2, lang => 'en', order => 1});

say "reviews: " . Dumper($review_list);

my $depth = $reviews->depth('ru');
say "depth = $depth";

