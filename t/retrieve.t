use 5.012;
use Test::MockObject;
use lib::abs '../lib';
use GooglePlay::ReviewBeta;
use Data::Dumper;
use File::Slurp;
use Mojo::JSON qw/decode_json encode_json/;
use Test::More;
use FindBin '$Bin';

my $ua = Test::MockObject->new();
Test::MockObject->fake_module('Mojo::UserAgent', new => sub { $ua });

$ua->mock( 'post', sub { return shift;});
$ua->mock( 'result', sub { return shift });
$ua->mock( 'body', sub { 
        my $body = read_file("$Bin/body.txt");
        return $body;
});

my $reviews = GooglePlay::ReviewBeta->new('com.aita');
my $list = $reviews->retrieve();

my $data = read_file("$Bin/reviews.json") or die 'inconsistent test';
my $expected = decode_json($data);

is_deeply($list,$expected, 'retrive');
done_testing();

