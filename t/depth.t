use 5.012;
use Test::MockObject;
use lib::abs '../lib';
use GooglePlay::ReviewBeta;
use File::Slurp;
use Data::Dumper;
use Test::More;
use FindBin '$Bin';

my $ua = Test::MockObject->new();
Test::MockObject->fake_module('Mojo::UserAgent', new => sub { $ua });

$ua->mock( 'post', sub { return shift;});
$ua->mock( 'result', sub { return shift });
$ua->mock( 'body', sub { 
        my $self = shift;
        my $fname = $self->{next} ? "$Bin/body.txt" : "$Bin/body-next.txt";
        my $body = read_file($fname); 
        $self->{next} = 1;
        return $body; 
});

my $reviews = GooglePlay::ReviewBeta->new('com.aita');
my $depth = $reviews->depth();
say $depth;

is($depth, 2, 'depth is rigth');

done_testing();
