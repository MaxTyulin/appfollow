package GooglePlay::ReviewBeta;

use 5.012;
use warnings;
use Mojo::UserAgent;
use Mojo::JSON qw/decode_json encode_json/;
use Data::Dumper;
use DateTime;

my $play_url = 'https://play.google.com/_/PlayStoreUi/data?ds.extension=136880256&f.sid=-5732833460201058032&soc-app=121&soc-platform=1&soc-device=1&authuser&_reqid=251464&rt=c';

my $params = { 
    'f.req' => [
        [
            [
                136880256,
                [
                    {
                        "136880256" => [
                            undef,
                            undef,
                            [
                                2,
                                undef, #1 - most helpful; 2 - newest; 3 - rating 
                                [
                                    40,
                                    undef, #next token
                                ]
                            ],
                            [
                                "", #ext_id
                                7
                            ]
                        ]
                    }
                ],
                undef,
                undef,
                0
            ]
        ]
    ],
    'at' => 'AFdERMLdVtwIx2PWdBbqu8ReYUxP:1524478610719',
};

sub new {
    my ($class, $ext_id) = @_;
    die 'ext_id is not specified' unless $ext_id;

    $params->{'f.req'}[0][0][1][0]{136880256}[3][0] = $ext_id;

    my $self = bless {}, $class;
    $self->{reviews} = [];

    return $self;
}

sub retrieve {
    my ($self, $args) = @_;
    die 'invalid args' if ($args && ref $args ne 'HASH');

    $self->{page} = $args && $args->{page};
    $self->{lang} = $args && $args->{lang} || 'ru'; 
    
    $params->{'f.req'}[0][0][1][0]{136880256}[2][1] = $args->{order} || 1;;

    $play_url .= '&hl='. $self->{lang};
    say $play_url;

    $self->{reviews} = [];
    $self->_get_next();

    return $self->{page} ? $self->{reviews}[$self->{page}-1] : $self->{reviews};
}


sub depth {
    my ($self, $lang) = @_;

    $self->{page} = undef;
    $self->{lang} = $lang || 'ru';

    $play_url .= '&hl='. $self->{lang};
    $self->{reviews} = [];

    $self->_get_next;
    return scalar @{$self->{reviews}};
}

sub _get_next {
    my $self = shift;

    my $ua = Mojo::UserAgent->new();
    my $tx = $ua->post($play_url => form => {'f.req' => encode_json($params->{'f.req'}), 'at' => $params->{'at'}});

    my $result = $tx->result;
    my $body = $result->body;
    #say $body;

    substr($body,0,6) = ""; #remove stuff from beginning
    my $length = $1 if ($body =~ s/^(\d+)//);
    $body =~ s/\d+\n\[\[\"di\",\d+.+$//sm; #remove rest from the end
    #say $body;

    say 'empty length',return unless $length;
    say "length: $length";

    my $json = eval { decode_json($body) };
    say "error decode json for body $body" unless $json;
    push @{$self->{reviews}}, $self->parse_response($json);

    say "page: " . scalar @{$self->{reviews}};

    my $next = $json->[0][2]{136880256}[1][0];
    if ($next && (
            !$self->{page} || 
            ($self->{page} > scalar @{$self->{reviews}})
        )
    ) {
        say "next: $next";
        $params->{'f.req'}[0][0][1][0]{136880256}[2][2][1] = $next;
        $self->_get_next();
    }
}

sub parse_response {
    my ($self, $response) = @_;
    my $reviews = $response->[0][2]{136880256}[0];

    my $data = [];
    for my $review (@$reviews) {
        my $dt = $review->[5][0] && DateTime->from_epoch(epoch => $review->[5][0]);
        push @$data, {
            review_id      => $review->[0],
            title          => $review->[1][0], #not sure
            content        => $review->[4],
            date           => $dt ? $dt->date : '',
            time           => $dt ? $dt->time : '',
            lang           => $self->{lang},
            author         => $review->[1][0],
            author_icon    => $review->[1][1][3][2],
            rating         => $review->[2],
            thumb_up_cnt   => $review->[6],
            thumb_down_cnt => '', #?

            #developers answer may be absent
            reply_content => $review->[7] && $review->[7][1],
            reply_date    => $review->[7] && DateTime->from_epoch(epoch => $review->[7][2][0])->date,
            is_reply      => $review->[7] ? 1 : 0,

        };
    }
    return $data;
}

1;
